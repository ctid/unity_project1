﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallCollisionEmitter : MonoBehaviour {

    public delegate void OnCollidedCallBack(Collision2D collision);

    public OnCollidedCallBack OnCollided;

    void OnCollisionEnter2D(Collision2D collision) {
        Debug.Log("OnColliderEnter2D");
        OnCollided(collision);
    }
}
