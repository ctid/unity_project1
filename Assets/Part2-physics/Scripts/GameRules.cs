﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameRules : MonoBehaviour {

    [Serializable]
    public struct Entity {
        public GameObject gameObject;
        [HideInInspector]
        public Vector3 initialPosition;
    }

    [Serializable]
    public struct Team {
        public Text score;
        public GameObject goal;
    }

    public Entity[] players;
    public Entity ball;
    public Team orange;
    public Team blue;

    private int orangeScore = 0;
    private int blueScore = 0;

    // Start is called before the first frame update
    void Start() {

        this.RecoreInitialPositions();
        this.UpdateScores();

        BallCollisionEmitter emitter = this.ball.gameObject.GetComponentInChildren<BallCollisionEmitter>();

        Debug.Log("Start -> Emmiter = " + emitter.name);
        emitter.OnCollided += this.BallCollided;

    }

    private void BallCollided(Collision2D collision) {
        Debug.Log("Collider = " + collision.collider.gameObject.name);
        Debug.Log("Other collider = " + collision.otherCollider.gameObject.name);
        if (collision.collider.gameObject.name == this.orange.goal.name) {
            this.orangeScore ++;

            this.UpdateScores();
            this.ResetPositions();
        }

        if (collision.collider.gameObject.name == this.blue.goal.name) {
            this.blueScore++;

            this.UpdateScores();
            this.ResetPositions();
        }
    }

    private void ResetPositions() {
        for (int i = 0; i < this.players.Length; i++) {
            this.players[i].gameObject.transform.position = this.players[i].initialPosition;
        }

        this.ball.gameObject.transform.position = this.ball.initialPosition;
        this.ball.gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;

    }

    public void UpdateScores() {
        this.orange.score.text = this.orangeScore.ToString();
        this.blue.score.text = this.blueScore.ToString();
    }

    public void RecoreInitialPositions() {
        for (int i = 0; i < this.players.Length; i++) {
            this.players[i].initialPosition = this.players[i].gameObject.transform.position;
        }

        this.ball.initialPosition = this.ball.gameObject.transform.position;
    }

    // Update is called once per frame
    void Update() {
        
    }
}
